package com.example.acer.pedometer_stepup;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;

/**
 * @author Suranjan Jena and Deepak Mishra
 * @since 14 Nov,2016
 * @version 1.0
 * The class displays the Start screen of the application.
 */
public class StartScreen extends AppCompatActivity {
    /**
     * Setting the layout of the Start Screen.
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_start_screen);
    }
    /**
     * Going to the Home Screen on button press
     * @param view
     */
    public void nextActivity(View view) {
        // Do something in response to button
        Intent intent = new Intent(this, Home.class);
        startActivity(intent);
    }
}
