package com.example.acer.pedometer_stepup;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.io.FileOutputStream;
import java.io.IOException;
/**
 * @author Suranjan Jena and Deepak Mishra
 * @since 14 Nov,2016
 * @version 1.0
 * The UserData class gets the information entered by the user and stores them to a file for use.
 */
public class UserData extends AppCompatActivity {
    /**
     * To provide the gender options to the user.
     */
    private Spinner gender;
    /**
     *To provide the gender options to the user
     */
    private String[] state = { "Male", "Female"};
    /**
     * Variable to store the gender
     */
    private String gen;
    /**
     * Variable to store the height
     */
    private double height;
    /**
     * Variable to store the weight
     */
    private double weight;
    /**
     * Variable to store the age
     */
    private int age;
    /**
     * To get the height from the user
     */
    private EditText ht;
    /**
     * To get the weight from the user
     */
    private EditText wt;
    /**
     * To get the age from the user
     */
    private EditText ag;
    /**
     * To display the BMI to the user
     */
    private TextView BMI;
    /**
     * Initializes the layout and the variables
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_data);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        wt=(EditText)findViewById(R.id.weight);
        ht=(EditText)findViewById(R.id.height);
        ag=(EditText)findViewById(R.id.age);
        BMI=(TextView)findViewById(R.id.BMI);

        setSupportActionBar(toolbar);
        gender=(Spinner) findViewById(R.id.spinner);
        /**
         * initializing the drop down option
         */
        ArrayAdapter<String> adapter_state = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_item, state);

        adapter_state.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        gender.setAdapter(adapter_state);

    }

    /**
     * This method is invoked when the ok button is pressed by the user
     * @param v
     * @throws IOException
     */
    public void getValues(View v) throws IOException {
        if(String.valueOf(wt.getText()).equals("")||String.valueOf(ht.getText()).equals("")||String.valueOf(ag.getText()).equals("")){
            Toast.makeText(getApplicationContext(), "PLEASE ENTER ALL THE FIELDS", Toast.LENGTH_LONG).show();
        }
        /**
         * Displays the BMI and writes the user data to a file.
         */
        else{
            weight=Double.parseDouble(String.valueOf(wt.getText()));
            height=Double.parseDouble(String.valueOf(ht.getText()));
            age=Integer.parseInt(String.valueOf(ag.getText()));
            gen = (String) gender.getSelectedItem();
            write("userData");
            /**
             * Displays the BMI to the user
             */
            BMI.setText("" + weight / (height * height));
            if(weight/(height*height)<18.5)
                Toast.makeText(getApplicationContext(),"You are under weight", Toast.LENGTH_LONG).show();
            else if((weight/(height*height)>=18.5)&&(weight/(height*height)<=24.9))
                Toast.makeText(getApplicationContext(),"You have a normal weight", Toast.LENGTH_LONG).show();
            else if((weight/(height*height)>=25.0))
                Toast.makeText(getApplicationContext(),"You are overweight", Toast.LENGTH_LONG).show();


        }}

    /**
     * This method writes the user data to the file
     * @param file
     * @throws IOException
     */
    public void write(String file) throws IOException {
        FileOutputStream f= openFileOutput(file,MODE_PRIVATE);
        f.write((String.valueOf(weight)+'\n'+String.valueOf(height)+'\n'+gen+'\n'+String.valueOf(age)).getBytes());
        f.close();
    }

    /**
     * Initializes the menu bar
     * @param menu
     * @return true
     */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_userdata, menu);
        return true;
    }

    /**
     * Navigates through the menu bar
     * @param item
     * @return
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.menu1) {
            Intent intent = new Intent(this, Home.class);
            if(String.valueOf(wt.getText()).equals("")||String.valueOf(ht.getText()).equals("")||String.valueOf(ag.getText()).equals("")){
                Toast.makeText(getApplicationContext(), "PLEASE ENTER ALL THE FIELDS", Toast.LENGTH_LONG).show();
            }
                else{
                startActivity(intent);
                return true;}
        }
        if (id == R.id.menu2) {
            Intent intent = new Intent(this, UserData.class);
            startActivity(intent);
            return true;
        }
        if (id == R.id.menu3) {
            Intent intent = new Intent(this, UserProgress.class);
            if(String.valueOf(wt.getText()).equals("")||String.valueOf(ht.getText()).equals("")||String.valueOf(ag.getText()).equals("")){
                Toast.makeText(getApplicationContext(), "PLEASE ENTER ALL THE FIELDS", Toast.LENGTH_LONG).show();
            }
            else{
                startActivity(intent);
                return true;}
        }
        return super.onOptionsItemSelected(item);
    }

}
