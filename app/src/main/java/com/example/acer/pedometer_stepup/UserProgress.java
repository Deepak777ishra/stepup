package com.example.acer.pedometer_stepup;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.jjoe64.graphview.GraphView;
import com.jjoe64.graphview.series.DataPoint;
import com.jjoe64.graphview.series.LineGraphSeries;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * @author Suranjan Jena and Deepak Mishra
 * @since 14 Nov,2016
 * @version 1.0
 * The class UserProgress charts the progress of the user over the last 15 workout.
 */
public class UserProgress extends AppCompatActivity {
    /**
     * Index to the data array.
     */
    private int i;
    /**
     * Variable to save the number of steps pf user over the past 15 workouts.
     */
    private int data[];
    /**
     * Initializes the layout
     * @param savedInstanceState
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_progress);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        /**
         * initializing the value of index to 14.
         */
        i=14;
        /**
         * initializing the data array.
         */
        data=new int[i+1];
        /**
         * reading the workout file.
         */
        try {
            read("workout");
        } catch (IOException e) {
            e.printStackTrace();
        }
        /**
         * Declaring the GraphView object to chart the user's progress
         */
        GraphView graph = (GraphView) findViewById(R.id.graph);
        /**
         * Plotting a linear graph of the number of workouts to the number of steps.
         */
        LineGraphSeries<DataPoint> series = new LineGraphSeries<DataPoint>(new DataPoint[] {
                new DataPoint(0, data[0]),
                new DataPoint(1, data[1]),
                new DataPoint(2, data[2]),
                new DataPoint(3, data[3]),
                new DataPoint(4, data[4]),
                new DataPoint(5, data[5]),
                new DataPoint(6, data[6]),
                new DataPoint(7, data[7]),
                new DataPoint(8, data[8]),
                new DataPoint(9, data[9]),
                new DataPoint(10, data[10]),
                new DataPoint(11, data[11]),
                new DataPoint(12, data[12]),
                new DataPoint(13, data[13]),
                new DataPoint(14, data[14])
        });
        graph.addSeries(series);
    }

    /**
     * Initializes the menu bar.
     * @param menu
     * @return true
     */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_userprogress, menu);
        return true;
    }

    /**
     * Navigates through the menu bar
     * @param item
     * @return true
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();
        /**
         * Navigates to home
         */
        if (id == R.id.menu1) {
            Intent intent = new Intent(this, Home.class);
            startActivity(intent);
            return true;
        }
        /**
         * Navigates to the user entry screen.
         */
        if (id == R.id.menu2) {
            Intent intent = new Intent(this, UserData.class);
            startActivity(intent);
            return true;
        }
        /**
         * Navigates to the user progress screen.
         */
        if (id == R.id.menu3) {
            Intent intent = new Intent(this, UserProgress.class);
            startActivity(intent);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    /**
     * The read method reads the user data from the file and initializes it to the data array.
     * @param file Name of the file.
     * @throws IOException
     */
    public void read(String file) throws IOException {
        /**
         * To create a new file in case the file does not exist.
         */
        FileOutputStream f_test = openFileOutput(file, MODE_APPEND);
        f_test.close();
        /**
         * Reading from the file.
         */
        try {
            FileInputStream f=openFileInput(file);
            BufferedReader br=new BufferedReader(new InputStreamReader(f));
            StringBuffer s=new StringBuffer();
            String m;

            while(((m=br.readLine())!=null)&&(i>=0)){
                s.append(m+'\n');
                data[i]=Integer.parseInt(m);
                i--;
            }

            f.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
